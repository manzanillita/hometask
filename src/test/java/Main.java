import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.apache.commons.lang3.RandomStringUtils;

public class Main {



    WebDriver driver;
    String baseUrl = "http://1:1@provse.wezom.net/";

    @Test
    public void initializeBrowser() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\Kislinskiy\\Automation\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseUrl);
        Actions abc = new Actions(driver);
        WebElement element = driver.findElement(By.xpath("html/body/div[1]/header/div[2]/div/div/div[2]/div/div[1]/a"));
        abc.moveToElement(element).build().perform();
        driver.findElement(By.xpath("html/body/div[1]/header/div[2]/div/div/div[2]/div/div[1]/div/ul/li[1]/a")).click();
        Thread.sleep(100);
        driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[1]/label[1]/div[2]/input")).sendKeys("palenaya.v.wezom@gmail.com");
        driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div/div[2]/div[1]/label[2]/div[2]/input")).sendKeys("123456");
        driver.findElement(By.xpath("html/body/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/label[1]/span[1]")).click();
        driver.findElement(By.xpath("html/body/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[2]/button[1]")).click();
        Thread.sleep(5000);
        driver.close();
    }
}
